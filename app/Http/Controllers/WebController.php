<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ContactUsRequest;

class WebController extends Controller
{
    
    public function getHome()
    {
        return view('index');
    }

    public function getMenu()
    {
        return view('menu');
    }

    public function getAboutUs()
    {
        return view('about-us');
    }

    public function getBook()
    {
        return view('book');        
    }

    public function getContactUs()
    {
        return view('contact-us');
    }
    public function postContactUs(ContactUsRequest $request)
    {
        $name = $request->get('name');
        $email = $request->get('email');
        $phone = $request->get('phone');
        $message = $request->get('message');

        $body = '';
        $body .= 'Nama: "'.$name.'"\r\n';
        $body .= 'Email: "'.$email.'"\r\n';
        $body .= 'Nomor HP: "'.$phone.'"\r\n';
        $body .= '"'.$message.'"';
        $body .= '';
        $check = mail('mimilodho@gmail.com', 'Kontak Kami', $body);
        
        $arr = 'sent';
        \Log::info(json_encode($arr));
        return json_encode($arr);
    }
}
