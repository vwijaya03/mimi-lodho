
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Mimi Lodho</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Mimi Lodho">
    <meta name="keywords" content="Mimi Lodho">
    <meta name="author" content="Mimi Lodho">


    <!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<![endif]-->


    <!-- CSS Files
    ================================================== -->
    <link rel="stylesheet" href="{{ URL::asset('public/css/bootstrap.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('public/css/animate.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('public/css/plugin.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('public/css/owl.carousel.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('public/css/owl.theme.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('public/css/owl.transitions.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('public/css/magnific-popup.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('public/css/style.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('public/demo/demo.css') }}" type="text/css">

    <!-- custom background -->
    <link rel="stylesheet" href="{{ URL::asset('public/css/bg.css') }}" type="text/css">

    <!-- revolution slider custom css -->
    <link rel="stylesheet" href="{{ URL::asset('public/css/rev-settings.css') }}" type="text/css">

    <!-- color scheme -->
    <link rel="stylesheet" href="{{ URL::asset('public/css/color.css') }}" type="text/css" id="colors">

    <!-- load fonts -->
    <link rel="stylesheet" href="{{ URL::asset('public/fonts/font-awesome/css/font-awesome.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('public/fonts/elegant_font/HTML_CSS/style.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('public/fonts/et-line-font/style.css') }}" type="text/css">
</head>
<body class="page-about">

    <div id="wrapper">
        <!-- header begin -->
        <header class="header-solid header-light">
            <div class="info">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="col"><span class="id-color"><i class="fa fa-map-marker"></i></span>Collins Street West, Victoria 8007 Australia </div>
                            <div class="col"><span class="id-color"><i class="fa fa-clock-o"></i></span>Monday - Friday 08:00-16:00</div>
                            <div class="col"><span class="id-color"><i class="fa fa-phone"></i></span>1800.899.900</div>
                            <div class="col">
                                <div id="lang-selector" class="dropdown">
                                    <a href="#" class="btn-selector">English</a>
                                    <ul>
                                        <li class="active"><a href="#">English</a></li>
                                        <li><a href="#">France</a></li>
                                        <li><a href="#">Germany</a></li>
                                        <li><a href="#">Spain</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 text-right">
                            <!-- social icons -->
                            <div class="col social">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-rss"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                                <a href="#"><i class="fa fa-envelope-o"></i></a>
                            </div>
                            <!-- social icons close -->
                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <!-- logo begin -->
                        <div id="logo">
                            <a href="index.html">
                                <img class="logo-2" src="{{ URL::asset('public/images/logo-2.png') }}" alt="">
                            </a>
                        </div>
                        <!-- logo close -->

                        <!-- small button begin -->
                        <span id="menu-btn"></span>
                        <!-- small button close -->

                        <!-- mainmenu begin -->
                        <nav>
                            <ul id="mainmenu">
                                <li><a href="{{ url('/') }}">Halaman Utama</a>
                                </li>
                                <li><a href="{{ url('/menu') }}">Menu</a>
                                </li>
                                <li><a href="{{ url('/about-us') }}">Tentang Kami</a></li>
                                <li><a href="{{ url('/book') }}">Pemesanan</a></li>
                                <!-- <li><a href="event.html">Events</a></li>
                                <li><a href="blog.html">Blog</a></li> -->
                                <li><a href="{{ url('/contact-us') }}">Kontak Kami</a></li>
                            </ul>
                        </nav>

                    </div>
                    <!-- mainmenu close -->

                </div>
            </div>
        </header>
        <!-- header close -->

        <!-- subheader -->
        <section id="subheader" data-speed="8" data-type="background">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>Tentang Kami</h1>
                        <h2><span></span></h2>

                    </div>
                </div>
            </div>
        </section>
        <!-- subheader close -->

        <!-- content begin -->
        <div id="content" class="no-top no-bottom">

            <!-- section begin -->
            <section id="sub-about-1" class="side-bg">
                <div class="col-md-6 image-container">
                    <div class="background-image"></div>
                </div>

                <div class="container">
                    <div class="row">
                        <div class="col-md-5 col-md-offset-7">

                            <h2>Selamat Datang Di MIMI LODHO<span class="teaser"></span><span class="tiny-border"></span></h2>

                            <p>
                                Dibuat dari bahan dasar ayam kampung segar pilihan dan bumbu warisan leluhur diacara - acara besar Tulungagung terciptalah MIMI LODHO, menawarkan sensasi rasa ayam lodho yg belum pernah anda rasakan sebelumnya.Kami hanya melayani pesanan.
                            </p>

                        </div>


                    </div>
                </div>
            </section>
            <!-- section close -->

            <!-- section begin -->
            <!-- <section id="sub-about-2" class="side-bg">
                <div class="image-container col-md-6 col-md-offset-6 pull-right">
                    <div class="background-image"></div>
                </div>

                <div class="container">
                    <div class="row">
                        <div class="col-md-5">

                            <h2>Restaurant<span class="teaser">Cozy &amp; Romantic</span><span class="tiny-border"></span></h2>

                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            </p>

                        </div>


                    </div>
                </div>
            </section> -->
            <!-- section close -->

            <!-- section begin -->
            <!-- <section id="section-fun-facts" class="bg-color text-light no-top no-bottom">
                <div class="pt20 pb20">
                    <div class="container bg-color">

                        <div class="row">
                            <div class="col-md-3 wow fadeIn" data-wow-delay="0">
                                <div class="de_count">
                                    <h3 class="timer" data-to="8350" data-speed="2500">0</h3>
                                    <span>Hours of Works</span>
                                </div>
                            </div>

                            <div class="col-md-3 wow fadeIn" data-wow-delay=".25s">
                                <div class="de_count">
                                    <h3 class="timer" data-to="248" data-speed="2500">0</h3>
                                    <span>Projects Complete</span>
                                </div>
                            </div>

                            <div class="col-md-3 wow fadeIn" data-wow-delay=".5s">
                                <div class="de_count">
                                    <h3 class="timer" data-to="852" data-speed="2500">0</h3>
                                    <span>Slice of Pizza</span>
                                </div>
                            </div>

                            <div class="col-md-3 wow fadeIn" data-wow-delay=".75s">
                                <div class="de_count">
                                    <h3 class="timer" data-to="615" data-speed="2500">0</h3>
                                    <span>Cups of Coffee</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section> -->
            <!-- section close -->

            <!-- <section>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <h2>Meet<span class="teaser">Our Team</span><span class="small-border center"></span></h2>
                        </div>

                        <div class="col-md-4">
                            <div class="profile_pic">
                                <figure class="pic-hover hover-scale mb30">
                                    <img src="images/team/team_pic_1.jpg" class="img-responsive" alt="">
                                </figure>

                                <h3>Ben Sheridan</h3>
                                <span class="subtitle">Founder & CEO</span>
                                <span class="tiny-border"></span>
                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo.
									<br>
                                <a href="#" class="read_more mt10">read more <i class="fa fa-chevron-right id-color"></i></a>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="profile_pic">
                                <figure class="pic-hover hover-scale mb30">
                                    <img src="images/team/team_pic_2.jpg" class="img-responsive" alt="">
                                </figure>
                                <h3>Sophie Lana</h3>
                                <span class="subtitle">Founder & CEO</span>
                                <span class="tiny-border"></span>
                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo.
									<br>
                                <a href="#" class="read_more mt10">read more <i class="fa fa-chevron-right id-color"></i></a>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="profile_pic">
                                <figure class="pic-hover hover-scale mb30">
                                    <img src="images/team/team_pic_3.jpg" class="img-responsive" alt="">
                                </figure>
                                <h3>James Hoult</h3>
                                <span class="subtitle">Project Manager</span>
                                <span class="tiny-border"></span>
                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo.
									<br>
                                <a href="#" class="read_more mt10">read more <i class="fa fa-chevron-right id-color"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </section> -->

        </div>

        <!-- footer begin -->
        <footer>
            <div class="container">
                <div class="row">
                    <div class="container">
                        <div class="col-md-4">
                            &copy; Copyright {{date('Y')}} - MIMI LODHO. <br> Developed by Viko Wijaya                     
                        </div>
                        <div class="col-md-4 text-center">
                            <img class="logo" src="{{ URL::asset('public/images/logo.png') }}" alt="">
                        </div>
                        <div class="col-md-4 text-right">
                            <div class="social-icons">
                                <a href="#"><i class="fa fa-facebook fa-lg"></i></a>
                                <a href="#"><i class="fa fa-twitter fa-lg"></i></a>
                                <a href="#"><i class="fa fa-rss fa-lg"></i></a>
                                <a href="#"><i class="fa fa-google-plus fa-lg"></i></a>
                                <a href="#"><i class="fa fa-skype fa-lg"></i></a>
                                <a href="#"><i class="fa fa-dribbble fa-lg"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- footer close -->

        <a href="#" id="back-to-top"></a>
        <div id="preloader">
            <div class="preloader1"></div>
        </div>
    </div>

    <!-- Javascript Files
    ================================================== -->
    <script src="{{ URL::asset('public/js/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('public/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('public/js/jquery.isotope.min.js') }}"></script>
    <script src="{{ URL::asset('public/js/easing.js') }}"></script>
    <script src="{{ URL::asset('public/js/jquery.flexslider-min.js') }}"></script>
    <script src="{{ URL::asset('public/js/jquery.scrollto.js') }}"></script>
    <script src="{{ URL::asset('public/js/owl.carousel.js') }}"></script>
    <script src="{{ URL::asset('public/js/jquery.countTo.js') }}"></script>
    <script src="{{ URL::asset('public/js/classie.js') }}"></script>
    <script src="{{ URL::asset('public/js/video.resize.js') }}"></script>
    <script src="{{ URL::asset('public/js/validation-reservation.js') }}"></script>
    <script src="{{ URL::asset('public/js/wow.min.js') }}"></script>
    <script src="{{ URL::asset('public/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ URL::asset('public/js/enquire.min.js') }}"></script>
    <script src="{{ URL::asset('public/js/jquery.stellar.min.js') }}"></script>
    <script src="{{ URL::asset('public/js/designesia.js') }}"></script>
    <script src="{{ URL::asset('public/demo/demo.js') }}"></script>

    <!-- RS5.0 Core JS Files -->
    <script type="text/javascript" src="{{ URL::asset('public/revolution/js/jquery.themepunch.tools.min.js?rev=5.0') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('public/revolution/js/jquery.themepunch.revolution.min.js?rev=5.0') }}"></script>

    <!-- RS5.0 Extensions Files -->
    <script type="text/javascript" src="{{ URL::asset('public/revolution/js/extensions/revolution.extension.video.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('public/revolution/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('public/revolution/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('public/revolution/js/extensions/revolution.extension.navigation.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('public/revolution/js/extensions/revolution.extension.actions.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('public/revolution/js/extensions/revolution.extension.kenburn.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('public/revolution/js/extensions/revolution.extension.migration.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('public/revolution/js/extensions/revolution.extension.parallax.min.js') }}"></script>

</body>
</html>