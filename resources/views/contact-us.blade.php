
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Mimi Lodho</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Mimi Lodho">
    <meta name="keywords" content="Mimi Lodho">
    <meta name="author" content="Mimi Lodho">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<![endif]-->


    <!-- CSS Files
    ================================================== -->
    <link rel="stylesheet" href="{{ URL::asset('public/css/bootstrap.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('public/css/animate.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('public/css/plugin.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('public/css/owl.carousel.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('public/css/owl.theme.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('public/css/owl.transitions.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('public/css/magnific-popup.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('public/css/style.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('public/demo/demo.css') }}" type="text/css">

    <!-- custom background -->
    <link rel="stylesheet" href="{{ URL::asset('public/css/bg.css') }}" type="text/css">

    <!-- color scheme -->
    <link rel="stylesheet" href="{{ URL::asset('public/css/color.css') }}" type="text/css" id="colors">

    <!-- revolution slider custom css -->
    <link rel="stylesheet" href="{{ URL::asset('public/css/rev-settings.css') }}" type="text/css">

    <!-- load fonts -->
    <link rel="stylesheet" href="{{ URL::asset('public/fonts/font-awesome/css/font-awesome.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('public/fonts/elegant_font/HTML_CSS/style.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('public/fonts/elegant_font/HTML_CSS/lte-ie7.js') }}" type="text/css">
</head>
<body class="page-contact">

    <div id="wrapper">
        <!-- header begin -->
        <header class="header-solid header-light">
            <div class="info">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="col"><span class="id-color"><i class="fa fa-map-marker"></i></span>Collins Street West, Victoria 8007 Australia </div>
                            <div class="col"><span class="id-color"><i class="fa fa-clock-o"></i></span>Monday - Friday 08:00-16:00</div>
                            <div class="col"><span class="id-color"><i class="fa fa-phone"></i></span>1800.899.900</div>
                            <div class="col">
                                <div id="lang-selector" class="dropdown">
                                    <a href="#" class="btn-selector">English</a>
                                    <ul>
                                        <li class="active"><a href="#">English</a></li>
                                        <li><a href="#">France</a></li>
                                        <li><a href="#">Germany</a></li>
                                        <li><a href="#">Spain</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 text-right">
                            <!-- social icons -->
                            <div class="col social">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-rss"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                                <a href="#"><i class="fa fa-envelope-o"></i></a>
                            </div>
                            <!-- social icons close -->
                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <!-- logo begin -->
                        <div id="logo">
                            <a href="index.html">
                                <img class="logo-2" src="{{ URL::asset('public/images/logo-2.png') }}" alt="">
                            </a>
                        </div>
                        <!-- logo close -->

                        <!-- small button begin -->
                        <span id="menu-btn"></span>
                        <!-- small button close -->

                        <!-- mainmenu begin -->
                        <nav>
                            <ul id="mainmenu">
                                <li><a href="{{ url('/') }}">Halaman Utama</a>
                                </li>
                                <li><a href="{{ url('/menu') }}">Menu</a>
                                </li>
                                <li><a href="{{ url('/about-us') }}">Tentang Kami</a></li>
                                <li><a href="{{ url('/book') }}">Pemesanan</a></li>
                                <!-- <li><a href="event.html">Events</a></li>
                                <li><a href="blog.html">Blog</a></li> -->
                                <li><a href="{{ url('/contact-us') }}">Kontak Kami</a></li>
                            </ul>
                        </nav>

                    </div>
                    <!-- mainmenu close -->

                </div>
            </div>
        </header>
        <!-- header close -->

        <!-- subheader -->
        <section id="subheader" data-speed="8" data-type="background">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>Kontak Kami</h1>
                        <h2><span></span></h2>

                    </div>
                </div>
            </div>
        </section>
        <!-- subheader close -->

        <!-- content begin -->
        <div id="content" class="no-top no-bottom">
            <section class="no-top no-bottom relative overflow-hidden">

                <div id="map" class="map-container-full">
                </div>

                <div class="color-overlay">



                    <div class="container">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2">
                                <div class="de_tab tab_style_2">
                                    <ul class="de_nav">
                                        <li class="active" data-wow-delay="0s"><span>Lokasi Kita</span><div class="v-border"></div>
                                        </li>
                                        <li><span>Kirim Pesan</span><div class="v-border"></div>
                                        </li>
                                        <li class="cust-map-toggle"><span>Lihat Di Peta</span><div class="v-border"></div>
                                        </li>
                                    </ul>

                                    <div class="de_tab_content tc_style-1">

                                        <div id="tab1">

                                            <div class="row">
                                                <div class="col-md-4 text-center">
                                                    <i class="icon_pin_alt fontsize48 id-color mb30"></i>
                                                    <h3>Alamat</h3>
                                                    GONG JAVA Coffee and Sport, Desa Tanjung, Kecamatan Kalidawir, Kabupaten Tulungagung, Jawa Timur, 66281
                                                </div>

                                                <div class="col-md-4 text-center">
                                                    <i class="icon_phone fontsize48 id-color mb30"></i>
                                                    <h3>Telp / WA</h3>
                                                    <a href="tel:081359549594">0813 5954 9594</a>
                                                </div>

                                                <div class="col-md-4 text-center">
                                                    <i class="icon_mail_alt fontsize48 id-color mb30"></i>
                                                    <h3>Email</h3>
                                                    <a href="mailto:mimilodho@gmail.com?subject=Kontak Mimi Lodho">mimilodho@gmail.com</a>
                                                </div>
                                            </div>

                                        </div>

                                        <div id="tab2">
                                            <div class="row">
                                                <div class="col-md-12">

                                                    <form name="contactForm" id='contact_form' method="post">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div id='name_error' class='error'>Please enter your name.</div>
                                                                <div>
                                                                    <input type='text' name='name' id='name' class="form-control" placeholder="Your Name">
                                                                </div>

                                                                <div id='email_error' class='error'>Please enter your valid E-mail ID.</div>
                                                                <div>
                                                                    <input type='text' name='email' id='email' class="form-control" placeholder="Your Email">
                                                                </div>

                                                                <div id='phone_error' class='error'>Please enter your phone number.</div>
                                                                <div>
                                                                    <input type='text' name='phone' id='phone' class="form-control" placeholder="Your Phone">
                                                                </div>

                                                                <div id='message_error' class='error'>Please enter your message.</div>
                                                                <div>
                                                                    <textarea name='message' id='message' class="form-control" placeholder="Your Message"></textarea>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-12 text-center">
                                                                <div id='submit'>
                                                                    <input type='submit' id='send_message' value='Submit Form' class="btn-solid rounded">
                                                                </div>
                                                                <div id='mail_success' class='success'>Your message has been sent successfully.</div>
                                                                <div id='mail_fail' class='error'>Sorry, error occured this time sending your message.</div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>


                                            </div>
                                        </div>



                                    </div>

                                </div>
                            </div>




                        </div>
                    </div>
                </div>


            </section>



            <!-- footer begin -->
            <footer>
                <div class="container">
                    <div class="row">
                        <div class="container">
                            <div class="col-md-4">
                                &copy; Copyright {{date('Y')}} - MIMI LODHO. <br> Developed by Viko Wijaya                     
                            </div>
                            <div class="col-md-4 text-center">
                                <img class="logo" src="{{ URL::asset('public/images/logo.png') }}" alt="">
                            </div>
                            <div class="col-md-4 text-right">
                                <div class="social-icons">
                                    <a href="#"><i class="fa fa-facebook fa-lg"></i></a>
                                    <a href="#"><i class="fa fa-twitter fa-lg"></i></a>
                                    <a href="#"><i class="fa fa-rss fa-lg"></i></a>
                                    <a href="#"><i class="fa fa-google-plus fa-lg"></i></a>
                                    <a href="#"><i class="fa fa-skype fa-lg"></i></a>
                                    <a href="#"><i class="fa fa-dribbble fa-lg"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- footer close -->

            <a href="#" id="back-to-top"></a>
            <div id="preloader">
                <div class="preloader1"></div>
            </div>
        </div>

        <!-- Javascript Files
    ================================================== -->
        <script src="{{ URL::asset('public/js/jquery.min.js') }}"></script>
        <script src="{{ URL::asset('public/js/bootstrap.min.js') }}"></script>
        <script src="{{ URL::asset('public/js/jquery.isotope.min.js') }}"></script>
        <script src="{{ URL::asset('public/js/easing.js') }}"></script>
        <script src="{{ URL::asset('public/js/jquery.flexslider-min.js') }}"></script>
        <script src="{{ URL::asset('public/js/jquery.scrollto.js') }}"></script>
        <script src="{{ URL::asset('public/js/owl.carousel.js') }}"></script>
        <script src="{{ URL::asset('public/js/jquery.countTo.js') }}"></script>
        <script src="{{ URL::asset('public/js/classie.js') }}"></script>
        <script src="{{ URL::asset('public/js/video.resize.js') }}"></script>
        <script src="{{ URL::asset('public/js/validation.js') }}"></script>
        <script src="{{ URL::asset('public/js/wow.min.js') }}"></script>
        <script src="{{ URL::asset('public/js/jquery.magnific-popup.min.js') }}"></script>
        <script src="{{ URL::asset('public/js/enquire.min.js') }}"></script>
        <script src="{{ URL::asset('public/js/jquery.stellar.min.js') }}"></script>
        <script src="{{ URL::asset('public/js/validation.js') }}"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDw5Em0O9I6M_FlHz7ZZpWci8j-JE35q_4"></script>
        <script src="{{ URL::asset('public/js/map.js') }}"></script>
        <script src="{{ URL::asset('public/js/designesia.js') }}"></script>
        <script src="{{ URL::asset('public/demo/demo.js') }}"></script>

        <!-- RS5.0 Core JS Files -->
        <script type="text/javascript" src="{{ URL::asset('public/revolution/js/jquery.themepunch.tools.min.js?rev=5.0') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('public/revolution/js/jquery.themepunch.revolution.min.js?rev=5.0') }}"></script>

        <!-- RS5.0 Extensions Files -->
        <script type="text/javascript" src="{{ URL::asset('public/revolution/js/extensions/revolution.extension.video.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('public/revolution/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('public/revolution/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('public/revolution/js/extensions/revolution.extension.navigation.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('public/revolution/js/extensions/revolution.extension.actions.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('public/revolution/js/extensions/revolution.extension.kenburn.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('public/revolution/js/extensions/revolution.extension.migration.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('public/revolution/js/extensions/revolution.extension.parallax.min.js') }}"></script>
</body>
</html>