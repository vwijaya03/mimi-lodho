<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Mimi Lodho</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Mimi Lodho">
    <meta name="keywords" content="Mimi Lodho">
    <meta name="author" content="Mimi Lodho">


    <!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<![endif]-->

    <!-- CSS Files
    ================================================== -->
    <link rel="stylesheet" href="{{ URL::asset('public/css/bootstrap.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('public/css/animate.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('public/css/plugin.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('public/css/owl.carousel.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('public/css/owl.theme.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('public/css/owl.transitions.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('public/css/magnific-popup.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('public/css/style.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('public/demo/demo.css') }}" type="text/css">

    <!-- custom background -->
    <link rel="stylesheet" href="{{ URL::asset('public/css/bg.css') }}" type="text/css">

    <!-- color scheme -->
    <link rel="stylesheet" href="{{ URL::asset('public/css/color.css') }}" type="text/css" id="colors">

    <!-- revolution slider custom css -->
    <link rel="stylesheet" href="{{ URL::asset('public/css/rev-settings.css') }}" type="text/css">

    <!-- load fonts -->\
    <link rel="stylesheet" href="{{ URL::asset('public/fonts/font-awesome/css/font-awesome.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('public/fonts/elegant_font/HTML_CSS/style.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('public/fonts/elegant_font/HTML_CSS/lte-ie7.js') }}" type="text/css">
</head>
<body class="page-menu">

    <div id="wrapper">
        <!-- header begin -->
        <header class="header-solid header-light">
            <div class="info">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="col"><span class="id-color"><i class="fa fa-map-marker"></i></span>Collins Street West, Victoria 8007 Australia </div>
                            <div class="col"><span class="id-color"><i class="fa fa-clock-o"></i></span>Monday - Friday 08:00-16:00</div>
                            <div class="col"><span class="id-color"><i class="fa fa-phone"></i></span>1800.899.900</div>
                            <div class="col">
                                <div id="lang-selector" class="dropdown">
                                    <a href="#" class="btn-selector">English</a>
                                    <ul>
                                        <li class="active"><a href="#">English</a></li>
                                        <li><a href="#">France</a></li>
                                        <li><a href="#">Germany</a></li>
                                        <li><a href="#">Spain</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 text-right">
                            <!-- social icons -->
                            <div class="col social">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-rss"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                                <a href="#"><i class="fa fa-envelope-o"></i></a>
                            </div>
                            <!-- social icons close -->
                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <!-- logo begin -->
                        <div id="logo">
                            <a href="index.html">
                                <img class="logo-2" src="{{ URL::asset('public/images/logo-2.png') }}" alt="">
                            </a>
                        </div>
                        <!-- logo close -->

                        <!-- small button begin -->
                        <span id="menu-btn"></span>
                        <!-- small button close -->

                        <!-- mainmenu begin -->
                        <nav>
                            <ul id="mainmenu">
                                <li><a href="{{ url('/') }}">Halaman Utama</a>
                                </li>
                                <li><a href="{{ url('/menu') }}">Menu</a>
                                </li>
                                <li><a href="{{ url('/about-us') }}">Tentang Kami</a></li>
                                <li><a href="{{ url('/book') }}">Pemesanan</a></li>
                                <!-- <li><a href="event.html">Events</a></li>
                                <li><a href="blog.html">Blog</a></li> -->
                                <li><a href="{{ url('/contact-us') }}">Kontak Kami</a></li>
                            </ul>
                        </nav>

                    </div>
                    <!-- mainmenu close -->

                </div>
            </div>
        </header>
        <!-- header close -->

        <!-- subheader -->
        <section id="subheader" data-speed="8" data-type="background">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>Menu</h1>
                        <h2><span>Segar &amp; Enak</span></h2>
                    </div>
                </div>
            </div>
        </section>
        <!-- subheader close -->

        <!-- content begin -->
        <div id="content">
            <div class="container">
                <div class="col-md-12">
                    <div class="de_tab tab_style_2">
                        <ul class="de_nav">
                            <li class="active" data-link="#section-services-tab"><span>Menu Kami</span><div class="v-border"></div>
                            </li>
                            <!-- <li data-link="#section-services-tab"><span>Starter</span><div class="v-border"></div>
                            </li>
                            <li data-link="#section-services-tab"><span>Drinks</span><div class="v-border"></div> -->
                        </ul>

                        <div class="de_tab_content">

                            <div id="tab1" class="tab_single_content">
                                    <div class="row">
                                        <div class="col-md-6 mb30">
                                            <div class="post-menu">
                                                <img src="{{ URL::asset('public/images/menu/thumbs-small/main/ayam_lodho.jpg') }}" class="img-responsive" alt="">
                                                <div class="sub-item-service meta">
                                                    <div class="c1">Lodho Ayam Kampung / Ekor</div>
                                                    <div class="c2"></div>
                                                    <div class="c3">Paket 1</div>
                                                </div>
                                                <div class="service-text meta-content">Hanya dari ayam kampung segar pilihan serta racikan bumbu - bumbu segar sehingga membuat rasa lodho kami memiliki citarasa tinggi, rasakan sensasi citarasa kuah yang gurih sampai tetes terakhir. Terdapat 2 pilihan nasi, nasi gurih dan nasi putih alami.<br>
                                                Paket untuk 5 orang <br>
                                                 ⁃ 1 ekor ayam kampung <br>
                                                 ⁃ Nasi putih/gurih 1 bakul (5 porsi) <br>
                                                 ⁃ Lalapan dan urap<br>
                                                 ⁃ Harga mulai Rp.120.000</div>
                                            </div>
                                        </div>

                                        <div class="col-md-6 mb30">
                                            <div class="post-menu">
                                                <img src="{{ URL::asset('public/images/menu/thumbs-small/main/ayam_arab.jpg') }}" class="img-responsive" alt="">
                                                <div class="sub-item-service meta">
                                                    <div class="c1">Lodho Ayam Arab / Ekor</div>
                                                    <div class="c2"></div>
                                                    <div class="c3">Paket 2</div>
                                                </div>
                                                <div class="service-text meta-content">Ayam arab atau ayam kedu (bukan ayam potong) adalah alternatif untuk lodho, mempunyai rasa yg mirip dg ayam kampung tapi selisih harga lebih murah. Terdapat 2 pilihan nasi, nasi gurih dan nasi putih alami.<br>
                                                Paket untuk 5 orang<br>
                                                 ⁃ 1 ekor ayam arab<br>
                                                 ⁃ Nasi putih/gurih 1 bakul (5 porsi)<br>
                                                 ⁃ Lalapan dan urap<br>
                                                 ⁃ Harga mulai Rp.85.000</div>
                                            </div>
                                        </div>

                                        <div class="col-md-6 mb30">
                                            <div class="post-menu">
                                                <img src="{{ URL::asset('public/images/menu/thumbs-small/main/ayam_bakar.jpg') }}" class="img-responsive" alt="">
                                                <div class="sub-item-service meta">
                                                    <div class="c1">Ayam Bakar Lalap</div>
                                                    <div class="c2"></div>
                                                    <div class="c3">Paket 3</div>
                                                </div>
                                                <div class="service-text meta-content">Dari ayam kampung pilihan, cara bakar serta racikan sambal yang membuat ayam bakar kami berbeda.<br>
                                                Paket untuk 5 orang<br>
                                                 ⁃ 1 ekor ayam kampung<br>
                                                 ⁃ Nasi 1 bakul / 5 porsi<br>
                                                 ⁃ Lalapan<br>
                                                 ⁃ Sambal terasi/sambal ijo<br>
                                                 ⁃ Harga mulai Rp.120.000</div>
                                            </div>
                                        </div>

                                        <div class="col-md-6 mb30">
                                            <div class="post-menu">
                                                <img src="{{ URL::asset('public/images/menu/thumbs-small/main/ayam_goreng.jpg') }}" class="img-responsive" alt="">
                                                <div class="sub-item-service meta">
                                                    <div class="c1">Ayam Goreng Lalap</div>
                                                    <div class="c2"></div>
                                                    <div class="c3">Paket 4</div>
                                                </div>
                                                <div class="service-text meta-content">Dari ayam kampung pilihan, cara menggoreng serta racikan sambal ala kampung Tulungagung yang membuat ayam goreng kami memiliki banyak kelebihan dibanding yang lain.<br>
                                                Paket untuk 5 orang <br>
                                                 ⁃ 1 ekor ayam kampung<br>
                                                 ⁃ Nasi 1 bakul / 5 porsi<br>
                                                 ⁃ Lalapan<br>
                                                 ⁃ Sambal terasi/ sambal ijo<br>
                                                 ⁃ Harga mulai Rp.100.000</div>
                                            </div>
                                        </div>

                                        <div class="col-md-6 mb30">
                                            <div class="post-menu">
                                                <img src="{{ URL::asset('public/images/menu/thumbs-small/main/lele.jpg') }}" class="img-responsive" alt="">
                                                <div class="sub-item-service meta">
                                                    <div class="c1">Lele Goreng Lalap</div>
                                                    <div class="c2"></div>
                                                    <div class="c3">Paket 5</div>
                                                </div>
                                                <div class="service-text meta-content">Hanya dari lele segar pilihan dan racikan unik sambal terasi serta lalapan khas kampung.<br>
                                                Paket untuk 1 orang (minimal order 5 paket) <br>
                                                 ⁃ 2 ekor lele <br>
                                                 ⁃ 1 piring nasi putih <br>
                                                 ⁃ Lalapan<br>
                                                 ⁃ Sambal terasi/sambal ijo<br>
                                                 ⁃ Harga mulai Rp. 10.000</div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-6 mb30">
                                            <div class="post-menu">
                                                <img src="{{ URL::asset('public/images/menu/thumbs-small/main/bandeng.jpg') }}" class="img-responsive" alt="">
                                                <div class="sub-item-service meta">
                                                    <div class="c1">Bandeng Goreng Lalap</div>
                                                    <div class="c2"></div>
                                                    <div class="c3">Paket 6</div>
                                                </div>
                                                <div class="service-text meta-content">Kalau selama ini anda kurang nyaman makan bandeng karena durinya, di sini anda akan menemukan perbedaan karena kami punya cara khusus menggoreng bandeng. Disajikan dg sambel plelek khas kami.<br>
                                                Paket untuk 1 orang (minimal order 5 paket) <br>
                                                 ⁃ 2 ekor ikan bandeng <br>
                                                 ⁃ 1 piring nasi putih <br>
                                                 ⁃ Lalapan<br>
                                                 ⁃ Sambal terasi/sambal ijo<br>
                                                 ⁃ Harga mulai Rp. 12.000</div>
                                            </div>
                                        </div>

                                        <div class="col-md-6 mb30">
                                            <div class="post-menu">
                                                <img src="{{ URL::asset('public/images/menu/thumbs-small/main/sapi_goreng.jpg') }}" class="img-responsive" alt="">
                                                <div class="sub-item-service meta">
                                                    <div class="c1">Daging Sapi Goreng Lalap</div>
                                                    <div class="c2"></div>
                                                    <div class="c3">Paket 7</div>
                                                </div>
                                                <div class="service-text meta-content">Tradisi kampung makan nasi putih pulen,daging sapi goreng dan sambel plelek akan menambah lengkap petualangan kuliner anda di Tulungagung.<br>
                                                Paket untuk 1 orang (minimal order 5 paket)<br>
                                                 ⁃ 1 ons daging sapi<br>
                                                 ⁃ 1 piring nasi putih<br>
                                                 ⁃ Lalapan<br>
                                                 ⁃ Sambel plelek/ sambel ijo<br>
                                                 ⁃ Harga mulai Rp.17.000</div>
                                            </div>
                                        </div>

                                        <div class="col-md-6 mb30">
                                            <div class="post-menu">
                                                <img src="{{ URL::asset('public/images/menu/thumbs-small/main/sapi_goreng.jpg') }}" class="img-responsive" alt="">
                                                <div class="sub-item-service meta">
                                                    <div class="c1">Nasi Campur</div>
                                                    <div class="c2"></div>
                                                    <div class="c3">Paket 8</div>
                                                </div>
                                                <div class="service-text meta-content">Menawarkan nasi campur daging dan nasi campur ayam kampung,semua kami sajikan ala kampung Tulungagung.<br>
                                                Paket untuk 1 orang (minimal order 10 paket)<br>
                                                 ⁃ Nasi putih<br>
                                                 ⁃ Ayam kampung/ daging<br>
                                                 ⁃ Sambal goreng dll<br>
                                                 ⁃ Harga mulai Rp.15.000</div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- footer begin -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="container">
                    <div class="col-md-4">
                        &copy; Copyright {{date('Y')}} - MIMI LODHO. <br> Developed by Viko Wijaya                     
                    </div>
                    <div class="col-md-4 text-center">
                        <img class="logo" src="{{ URL::asset('public/images/logo.png') }}" alt="">
                    </div>
                    <div class="col-md-4 text-right">
                        <div class="social-icons">
                            <a href="#"><i class="fa fa-facebook fa-lg"></i></a>
                            <a href="#"><i class="fa fa-twitter fa-lg"></i></a>
                            <a href="#"><i class="fa fa-rss fa-lg"></i></a>
                            <a href="#"><i class="fa fa-google-plus fa-lg"></i></a>
                            <a href="#"><i class="fa fa-skype fa-lg"></i></a>
                            <a href="#"><i class="fa fa-dribbble fa-lg"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- footer close -->

    <a href="#" id="back-to-top"></a>
    <div id="preloader">
        <div class="preloader1"></div>
    </div>
    </div>

    <!-- Javascript Files
    ================================================== -->
    <script src="{{ URL::asset('public/js/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('public/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('public/js/jquery.isotope.min.js') }}"></script>
    <script src="{{ URL::asset('public/js/easing.js') }}"></script>
    <script src="{{ URL::asset('public/js/jquery.flexslider-min.js') }}"></script>
    <script src="{{ URL::asset('public/js/jquery.scrollto.js') }}"></script>
    <script src="{{ URL::asset('public/js/owl.carousel.js') }}"></script>
    <script src="{{ URL::asset('public/js/jquery.countTo.js') }}"></script>
    <script src="{{ URL::asset('public/js/classie.js') }}"></script>
    <script src="{{ URL::asset('public/js/video.resize.js') }}"></script>
    <script src="{{ URL::asset('public/js/validation-reservation.js') }}"></script>
    <script src="{{ URL::asset('public/js/wow.min.js') }}"></script>
    <script src="{{ URL::asset('public/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ URL::asset('public/js/enquire.min.js') }}"></script>
    <script src="{{ URL::asset('public/js/jquery.stellar.min.js') }}"></script>
    <script src="{{ URL::asset('public/js/designesia.js') }}"></script>
    <script src="{{ URL::asset('public/demo/demo.js') }}"></script>

    <!-- RS5.0 Core JS Files -->
    <script type="text/javascript" src="{{ URL::asset('public/revolution/js/jquery.themepunch.tools.min.js?rev=5.0') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('public/revolution/js/jquery.themepunch.revolution.min.js?rev=5.0') }}"></script>

    <!-- RS5.0 Extensions Files -->
    <script type="text/javascript" src="{{ URL::asset('public/revolution/js/extensions/revolution.extension.video.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('public/revolution/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('public/revolution/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('public/revolution/js/extensions/revolution.extension.navigation.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('public/revolution/js/extensions/revolution.extension.actions.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('public/revolution/js/extensions/revolution.extension.kenburn.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('public/revolution/js/extensions/revolution.extension.migration.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('public/revolution/js/extensions/revolution.extension.parallax.min.js') }}"></script>

</body>
</html>