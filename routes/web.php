<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WebController@getHome')->name('getHome');
Route::get('/index', 'WebController@getHome')->name('getHome');
Route::get('/menu', 'WebController@getMenu')->name('getMenu');
Route::get('/about-us', 'WebController@getAboutUs')->name('getAboutUs');
Route::get('/book', 'WebController@getBook')->name('getBook');
Route::get('/contact-us', 'WebController@getContactUs')->name('getContactUs');
Route::post('/contact-us', 'WebController@postContactUs')->name('postContactUs');
